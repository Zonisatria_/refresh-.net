﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Aplicantform.Models
{
    public class PelamarModelAccess
    {
        public static string Message;
        public static List<PelamarModelView> GetListAll()
        {
            List<PelamarModelView> listData = new List<PelamarModelView>();
            using (var db = new applicant_dbEntities1())
            {
                listData = (from a in db.tb_pelamar
                            join b in db.tb_jurusan
                            on a.jurusan equals b.id
                            where a.is_delete == false
                            select new PelamarModelView
                            {
                                id = a.id,
                                created_by = a.created_by,
                                created_on = a.created_on,
                                modified_by = a.modified_by,
                                modified_on = a.modified_on,
                                deleted_by = a.deleted_by,
                                deleted_on = a.deleted_on,
                                is_delete = a.is_delete,
                                nama = a.nama,
                                no_hp = a.no_hp,
                                email = a.email,
                                tanggal_lahir = a.tanggal_lahir,
                                alamat = a.alamat,
                                jurusan = a.jurusan,
                                pertanyaan = a.pertanyaan,
                                jurusan_show = b.nama
                            }).ToList();
            }
            return listData;
        }


        public static PelamarModelView GetDetailById(int Id)
        {
            PelamarModelView result = new PelamarModelView();
            using (var db = new applicant_dbEntities1())
            {
                result = (from a in db.tb_pelamar
                          join b in db.tb_jurusan
                          on a.jurusan equals b.id
                          where a.id == Id
                          select new PelamarModelView
                          {
                              id = a.id,
                              created_by = a.created_by,
                              created_on = a.created_on,
                              modified_by = a.modified_by,
                              modified_on = a.modified_on,
                              deleted_by = a.deleted_by,
                              deleted_on = a.deleted_on,
                              is_delete = a.is_delete,
                              nama = a.nama,
                              no_hp = a.no_hp,
                              email = a.email,
                              tanggal_lahir = a.tanggal_lahir,
                              alamat = a.alamat,
                              jurusan = a.jurusan,
                              pertanyaan = a.pertanyaan,
                              jurusan_show = b.nama
                          }).FirstOrDefault();
            }
            return result;
        }


        public static List<PelamarModelView> GetJurusan()
        {
            List<PelamarModelView> listNote = new List<PelamarModelView>();
            using (var db = new applicant_dbEntities1())
            {
                listNote = (from a in db.tb_jurusan
                            where a.is_delete == false
                            select new PelamarModelView
                            {
                                id = a.id,
                                nama = a.nama
                            }).ToList();
            }
            return listNote;
        }




        public static bool Insert(PelamarModelView paramModel)
        {
            bool result = true;
            try
            {
                using (var db = new applicant_dbEntities1())
                {
                    tb_pelamar a = new tb_pelamar();
                    a.created_by = paramModel.created_by;
                    a.created_on = paramModel.created_on;
                    a.is_delete = paramModel.is_delete;
                    a.nama = paramModel.nama;
                    a.no_hp = paramModel.no_hp;
                    a.email = paramModel.email;
                    a.tanggal_lahir = paramModel.tanggal_lahir;
                    a.alamat = paramModel.alamat;
                    a.jurusan = paramModel.jurusan;
                    a.pertanyaan = paramModel.pertanyaan;

                    db.tb_pelamar.Add(a);
                    db.SaveChanges();
                }
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }



        public static bool Update(PelamarModelView paramModel)
        {
            bool result = true;

            try
            {
                using (var db = new applicant_dbEntities1())
                {
                    tb_pelamar a = db.tb_pelamar.Where(
                        o => o.id == paramModel.id).FirstOrDefault();

                    if (a != null)
                    {
                        a.modified_by = 1;
                        a.modified_on = DateTime.Now;
                        a.nama = paramModel.nama;
                        a.no_hp = paramModel.no_hp;
                        a.email = paramModel.email;
                        a.tanggal_lahir = paramModel.tanggal_lahir;
                        a.alamat = paramModel.alamat;
                        a.jurusan = paramModel.jurusan;
                        a.pertanyaan = paramModel.pertanyaan;
                        db.SaveChanges();
                    }
                    else
                    {
                        result = false;
                        Message = "Data Tidak Ditemukan";
                    }
                }
            }

            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }



        public static bool Delete(PelamarModelView paramModel)
        {
            bool result = true;

            try
            {
                using (var db = new applicant_dbEntities1())
                {
                    tb_pelamar a = db.tb_pelamar.Where(o => o.id == paramModel.id).FirstOrDefault();
                    if (a != null)
                    {
                        a.is_delete = true;
                        a.deleted_by = paramModel.deleted_by;
                        a.deleted_on = paramModel.deleted_on;

                        db.SaveChanges();
                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }

    }
}