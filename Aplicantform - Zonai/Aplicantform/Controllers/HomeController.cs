﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Aplicantform.Models;
using System.Text.RegularExpressions;


namespace Aplicantform.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            return PartialView(PelamarModelAccess.GetListAll());
        }

        public ActionResult Create()
        {
            ViewBag.ListJurusan = new SelectList(PelamarModelAccess.GetJurusan(), "id", "nama");
            return PartialView();
        }
        [HttpPost]
        public ActionResult Create(PelamarModelView paramModel)
        {
            try
            {
                paramModel.created_by = 1;
                paramModel.created_on = DateTime.Now;
                paramModel.is_delete = false;

                PelamarModelAccess.Message = string.Empty;

                //Validasi NO.HP(metode Regex)
                if (!Regex.IsMatch(paramModel.no_hp, @"^\d+$"))
                {
                    PelamarModelAccess.Message += "Format No. HP salah! ";
                }
                if (paramModel.no_hp.ToString().Length > 12)
                {
                    PelamarModelAccess.Message += "No. HP tidak boleh lebih dari 10! ";
                }

                //Validasi Email
                String[] val_email = paramModel.email.Split('@');
                if (val_email.Length == 1)
                {
                    PelamarModelAccess.Message += "Format email salah! ";
                }


                if (null == paramModel.nama || null == paramModel.no_hp || null == paramModel.tanggal_lahir || null == paramModel.alamat)
                {
                    PelamarModelAccess.Message += "Anda belum memasukan semua data. Silahkan ulang kembali";
                }
                if (string.IsNullOrEmpty(PelamarModelAccess.Message))
                {
                    return Json(new
                    {
                        success = PelamarModelAccess.Insert(paramModel),
                        message = PelamarModelAccess.Message
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = PelamarModelAccess.Message },
                    JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception hasError)
            {
                return Json(new { success = false, message = hasError.Message },
                    JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Detail(Int32 paramId)
        {
            return PartialView(PelamarModelAccess.GetDetailById(paramId));
        }



        public ActionResult Edit(Int32 paramId)
        {
            ViewBag.ListJurusan = new SelectList(PelamarModelAccess.GetJurusan(), "id", "nama");
            return PartialView(PelamarModelAccess.GetDetailById(paramId));
        }

        [HttpPost]
        public ActionResult Edit(PelamarModelView paramModel)
        {
            try
            {
                paramModel.modified_by = 1;
                paramModel.modified_on = DateTime.Now;

                PelamarModelAccess.Message = string.Empty;

                //Validasi NO.HP(metode Regex)
                if (!Regex.IsMatch(paramModel.no_hp, @"^\d+$"))
                {
                    PelamarModelAccess.Message += "Format No. HP salah! ";
                }
                if (paramModel.no_hp.ToString().Length > 12)
                {
                    PelamarModelAccess.Message += "No. HP tidak boleh lebih dari 10! ";
                }

                //Validasi Email
                String[] val_email = paramModel.email.Split('@');
                if (val_email.Length == 1)
                {
                    PelamarModelAccess.Message += "Format email salah! ";
                }

                if (null == paramModel.nama || null == paramModel.no_hp || null == paramModel.tanggal_lahir || null == paramModel.alamat)
                {
                    PelamarModelAccess.Message += "Anda belum memasukan semua data. Silahkan ulang kembali";
                }
                if (string.IsNullOrEmpty(PelamarModelAccess.Message))
                {
                    return Json(new
                    {
                        success = PelamarModelAccess.Update(paramModel),
                        message = PelamarModelAccess.Message
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = PelamarModelAccess.Message },
                    JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception hasError)
            {
                return Json(new
                {
                    success = false,
                    message = hasError.Message
                },
                JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult Delete(Int32 Id)
        {
            return PartialView(PelamarModelAccess.GetDetailById(Id));
        }

        [HttpPost]
        public ActionResult Delete(PelamarModelView paramModel)
        {
            try
            {
                paramModel.deleted_by = 1;
                paramModel.deleted_on = DateTime.Now;

                return Json(new
                {
                    success = PelamarModelAccess.Delete(paramModel),
                    message = PelamarModelAccess.Message
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception hasError)
            {
                return Json(new
                {
                    success = false,
                    message = hasError.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult Galeri()
        {
            return PartialView();
        }





    }
}