﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WFO.Models;
using System.Text.RegularExpressions;

namespace WFO.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()

        {
            return View();
        }
        public ActionResult List()
        {
            return PartialView(SalesModelAccess.GetListAll());
        }

        public ActionResult Create()
        {
            ViewBag.Position = new SelectList(SalesModelAccess.Position(), "id", "position_name");
            //ViewBag.PositionList = new SelectList(getPosition(), "Value", "Value");
            return PartialView();
        }
        [HttpPost]
        public ActionResult Create(TB_SALES paramModel)
        {
            try
            {
                SalesModelAccess.Message = string.Empty;

                paramModel.created_by = 1;
                paramModel.created_on = DateTime.Now;
                paramModel.is_delete = false;
             
                //Create NIK
                using (var db = new Model1())
                {
                    string nol = "";
                    TB_SALES cek = db.TB_SALES.OrderByDescending(x => x.NIK).First();
                    int simpan = int.Parse(cek.NIK.Substring(3));
                    simpan++;
                    for (int i = simpan.ToString().Length; i < 4; i++)
                    {
                        nol = nol + "0";
                    }
                    paramModel.NIK = "S" + nol + simpan;
                }

                if (null == paramModel.no_hp || null == paramModel.email || null == paramModel.dob)
                {
                    SalesModelAccess.Message = "Anda belum memasukan semua data. Silahkan ulang kembali ";
                }

                if (string.IsNullOrEmpty(SalesModelAccess.Message))
                {                    
                    //Validasi tahun lahir
                    DateTime lahir = paramModel.dob ?? DateTime.Now;
                    DateTime date_now = DateTime.Now;
                    DateTime tahun = lahir.AddYears(18);
                    if (date_now <= tahun)
                    {
                        SalesModelAccess.Message += "Umur anda belum 18 tahun keatas! ";
                    }

                    //Validasi NO.HP(metode Regex)
                    if (!Regex.IsMatch(paramModel.no_hp, @"^\d+$"))
                    {
                        SalesModelAccess.Message += "Format No. HP salah! ";
                    }
                    if (paramModel.no_hp.ToString().Length > 10)
                    {
                        SalesModelAccess.Message += "No. HP tidak boleh lebih dari 10! ";
                    }

                    //Validasi Email
                    String[] val_email = paramModel.email.Split('@');
                    if (val_email.Length == 1)
                    {
                        SalesModelAccess.Message += "Format email salah! ";
                    }                    
                }


                if (string.IsNullOrEmpty(SalesModelAccess.Message))
                {
                    return Json(new
                    {
                        success = SalesModelAccess.Insert(paramModel),
                        message = SalesModelAccess.Message
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = SalesModelAccess.Message },
                    JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception hasError)
            {
                return Json(new { success = false, message = hasError.Message },
                    JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult Detail(Int32 paramId)
        {
            return PartialView(SalesModelAccess.GetDetailById(paramId));
        }




        public ActionResult Edit(Int32 paramId)
        {
            ViewBag.Position = new SelectList(SalesModelAccess.Position(), "id", "position_name");
            //ViewBag.PositionList = new SelectList(getPosition(), "Value", "Value");
            return PartialView(SalesModelAccess.GetDetailById(paramId));
        }

        [HttpPost]
        public ActionResult Edit(TB_SALES paramModel)
        {
            try
            {
                SalesModelAccess.Message = string.Empty;

                paramModel.modified_by = 1;
                paramModel.modified_on = DateTime.Now;

                if (null == paramModel.no_hp || null == paramModel.email || null == paramModel.dob)
                {
                    SalesModelAccess.Message = "Anda belum memasukan semua data. Silahkan ulang kembali ";
                }

                if (string.IsNullOrEmpty(SalesModelAccess.Message))
                {
                    //Validasi tahun lahir
                    DateTime lahir = paramModel.dob ?? DateTime.Now;
                    DateTime date_now = DateTime.Now;
                    DateTime tahun = lahir.AddYears(18);
                    if (date_now <= tahun)
                    {
                        SalesModelAccess.Message += "Umur anda belum 18 tahun keatas! ";
                    }

                    //Validasi NO.HP(metode Regex)
                    if (!Regex.IsMatch(paramModel.no_hp, @"^\d+$"))
                    {
                        SalesModelAccess.Message += "Format No. HP salah! ";
                    }
                    if (paramModel.no_hp.ToString().Length > 10)
                    {
                        SalesModelAccess.Message += "No. HP tidak boleh lebih dari 10! ";
                    }

                    //Validasi Email
                    String[] val_email = paramModel.email.Split('@');
                    if (val_email.Length == 1)
                    {
                        SalesModelAccess.Message += "Format email salah! ";
                    }
                }


                if (string.IsNullOrEmpty(SalesModelAccess.Message))
                {
                    return Json(new
                    {
                        success = SalesModelAccess.Update(paramModel),
                        message = SalesModelAccess.Message
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, message = SalesModelAccess.Message },
                    JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception hasError)
            {
                return Json(new
                {
                    success = false,
                    message = hasError.Message
                },
                JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult Delete(Int32 Id)
        {
            return PartialView(SalesModelAccess.GetDetailById(Id));
        }

        [HttpPost]
        public ActionResult Delete(TB_SALES paramModel)
        {
            try
            {
                paramModel.deleted_by = 1;
                paramModel.deleted_on = DateTime.Now;

                return Json(new
                {
                    success = SalesModelAccess.Delete(paramModel),
                    message = SalesModelAccess.Message
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception hasError)
            {
                return Json(new
                {
                    success = false,
                    message = hasError.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }



        //public List<SelectListItem> getPosition()
        //{
        //    List<SelectListItem> getPosition = new List<SelectListItem>();
        //    getPosition.Add(new SelectListItem { Value = "Presales" });
        //    getPosition.Add(new SelectListItem { Value = "Sales" });
        //    return getPosition;
        //}

    }
}