﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WFO.Models
{
    public class PositionGetList
    {
        public long id { get; set; }

        public long created_by { get; set; }

        public DateTime created_on { get; set; }

        public long? modified_by { get; set; }

        public DateTime? modified_on { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_on { get; set; }

        public bool is_delete { get; set; }

        public string position_name { get; set; }

        public string deskripsi { get; set; }

    }
}