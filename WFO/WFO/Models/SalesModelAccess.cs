﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WFO.Models
{
    public class SalesModelAccess
    {
        public static string Message;
        public static List<SalesGetListAll> GetListAll()
        {
            List<SalesGetListAll> listData = new List<SalesGetListAll>();
            using (var db = new Model1())
            {
                listData = (from a in db.TB_SALES
                            join b in db.TB_POSITION
                            on a.position equals b.id
                            where a.is_delete == false
                            select new SalesGetListAll
                            {
                                id = a.id,
                                created_by = a.created_by,
                                created_on = a.created_on,
                                modified_by = a.modified_by,
                                modified_on = a.modified_on,
                                deleted_by = a.deleted_by,
                                deleted_on = a.deleted_on,
                                is_delete = a.is_delete,
                                name = a.name,
                                NIK = a.NIK,
                                bop = a.bop,
                                dob = a.dob,
                                alamat = a.alamat,
                                no_hp = a.no_hp,
                                email = a.email,
                                NIK_post = b.position_name
                            }).ToList();
            }
            return listData;
        }




        public static bool Insert(TB_SALES paramModel)
        {
            bool result = true;
            try
            {
                using (var db = new Model1())
                {
                    TB_SALES a = new TB_SALES();
                    a.created_by = paramModel.created_by;
                    a.created_on = paramModel.created_on;
                    a.is_delete = paramModel.is_delete;
                    a.name = paramModel.name;
                    a.NIK = paramModel.NIK;
                    a.bop = paramModel.bop;
                    a.dob = paramModel.dob;
                    a.alamat = paramModel.alamat;
                    a.no_hp = paramModel.no_hp;
                    a.email = paramModel.email;
                    a.position = paramModel.position;

                    db.TB_SALES.Add(a);
                    db.SaveChanges();
                }
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }

            return result;
        }


        public static List<PositionGetList> Position()
        {
            List<PositionGetList> listNote = new List<PositionGetList>();
            using (var db = new Model1())
            {
                listNote = (from a in db.TB_POSITION
                            where a.is_delete == false
                            select new PositionGetList
                            {
                                id = a.id,
                                position_name = a.position_name
                                

                            }).ToList();
            }
            return listNote;
        }



        public static SalesGetListAll GetDetailById(int Id)
        {
            SalesGetListAll result = new SalesGetListAll();
            using (var db = new Model1())
            {
                result = (from a in db.TB_SALES
                          join b in db.TB_POSITION
                          on a.position equals b.id
                          where a.id == Id
                          select new SalesGetListAll
                          {
                              id = a.id,
                              created_by = a.created_by,
                              created_on = a.created_on,
                              modified_by = a.modified_by,
                              modified_on = a.modified_on,
                              deleted_by = a.deleted_by,
                              deleted_on = a.deleted_on,
                              is_delete = a.is_delete,
                              name = a.name,
                              NIK = a.NIK,
                              bop = a.bop,
                              dob = a.dob,
                              alamat = a.alamat,
                              no_hp = a.no_hp,
                              email = a.email,
                              NIK_post = b.position_name,
                              position = a.position
                          }).FirstOrDefault();
            }
            return result;
        }



        public static bool Update(TB_SALES paramModel)
        {
            bool result = true;

            try
            {
                using (var db = new Model1())
                {
                    TB_SALES a = db.TB_SALES.Where(
                        o => o.id == paramModel.id).FirstOrDefault();

                    if (a != null)
                    {
                        a.modified_by = paramModel.modified_by;
                        a.modified_on = paramModel.modified_on;
                        a.name = paramModel.name;
                        a.bop = paramModel.bop;
                        a.dob = paramModel.dob;
                        a.alamat = paramModel.alamat;
                        a.no_hp = paramModel.no_hp;
                        a.email = paramModel.email;
                        a.position = paramModel.position;
                        db.SaveChanges();
                    }
                    else
                    {
                        result = false;
                        Message = "Data Tidak Ditemukan";
                    }
                }
            }

            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }




        public static bool Delete(TB_SALES paramModel)
        {
            bool result = true;

            try
            {
                using (var db = new Model1())
                {
                    TB_SALES a = db.TB_SALES.Where(o => o.id == paramModel.id).FirstOrDefault();
                    if (a != null)
                    {
                        a.is_delete = true;
                        a.deleted_by = paramModel.deleted_by;
                        a.deleted_on = paramModel.deleted_on;

                        db.SaveChanges();
                    }
                    else
                    {
                        result = false;
                    }
                }
            }
            catch (Exception hasError)
            {
                result = false;
                if (hasError.Message.ToLower().Contains("inner exception"))
                {
                    Message = hasError.InnerException.InnerException.Message;
                }
                else
                {
                    Message = hasError.Message;
                }
            }
            return result;
        }





    }
}