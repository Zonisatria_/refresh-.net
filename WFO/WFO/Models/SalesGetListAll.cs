﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WFO.Models
{
    public class SalesGetListAll
    {
        public long id { get; set; }
        public long created_by { get; set; }
        public System.DateTime created_on { get; set; }
        public Nullable<long> modified_by { get; set; }
        public Nullable<System.DateTime> modified_on { get; set; }
        public Nullable<long> deleted_by { get; set; }
        public Nullable<System.DateTime> deleted_on { get; set; }
        public bool is_delete { get; set; }
        public string name { get; set; }
        public string NIK { get; set; }
        public string NIK_post { get; set; }
        public string bop { get; set; }
        public Nullable<System.DateTime> dob { get; set; }
        public string alamat { get; set; }
        public string no_hp { get; set; }
        public string email { get; set; }
        public int position { get; set; }
    }
}