namespace WFO
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TB_SALES
    {
        public long id { get; set; }

        public long created_by { get; set; }

        public DateTime created_on { get; set; }

        public long? modified_by { get; set; }

        public DateTime? modified_on { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_on { get; set; }

        public bool is_delete { get; set; }

        [StringLength(50)]
        public string name { get; set; }

        [StringLength(5)]
        public string NIK { get; set; }

        [StringLength(50)]
        public string bop { get; set; }

        public DateTime? dob { get; set; }

        [StringLength(100)]
        public string alamat { get; set; }

        [StringLength(10)]
        public string no_hp { get; set; }

        [StringLength(30)]
        public string email { get; set; }

        public int position { get; set; }
    }
}
