namespace WFO
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<TB_POSITION> TB_POSITION { get; set; }
        public virtual DbSet<TB_SALES> TB_SALES { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TB_POSITION>()
                .Property(e => e.position_name)
                .IsUnicode(false);

            modelBuilder.Entity<TB_POSITION>()
                .Property(e => e.deskripsi)
                .IsUnicode(false);

            modelBuilder.Entity<TB_SALES>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<TB_SALES>()
                .Property(e => e.NIK)
                .IsUnicode(false);

            modelBuilder.Entity<TB_SALES>()
                .Property(e => e.bop)
                .IsUnicode(false);

            modelBuilder.Entity<TB_SALES>()
                .Property(e => e.alamat)
                .IsUnicode(false);

            modelBuilder.Entity<TB_SALES>()
                .Property(e => e.no_hp)
                .IsUnicode(false);

            modelBuilder.Entity<TB_SALES>()
                .Property(e => e.email)
                .IsUnicode(false);
        }
    }
}
